[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4686945.svg)](https://doi.org/10.5281/zenodo.4686945)

# Energy demand scenarios in industry until the year 2050 - ambitious policy scenario


## Repository structure
```
data                    -- containts the dataset in CSV format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```

## Documentation

The scenario data will be used in the Hotmaps toolbox to estimate potential developments of energy demand in EU28 in order to compare results on local developments with overal efficiency and emission mitigation targets on country and EU28 level.

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps: D5.2 Heating & Cooling outlook until 2050](http://www.hotmaps-project.eu/library/) 


## How to cite


Lukas Kranzl, Michael Hartner, Andreas Müller, Gustav Resch, Sara Fritz (TUW), Andreas Müller (e‐think),Tobias Fleiter, Andrea Herbst, Matthias Rehfeldt, Pia Manz (Fraunhofer ISI,)
Alyona Zubaryeva (EURAC), reviewed by Jakob Rager (CREM): Hotmaps Project, D5.2 Heating & Cooling outlook until 2050, EU-28, 2018 [www.hotmaps-project.eu](http://www.hotmaps-project.eu/library/) 


## Authors
Tobias Fleiter <sup>*</sup>,

<sup>*</sup> [Fraunhofer Institute for Systems and Innovation Research](https://www.isi.fraunhofer.de/en.html)  
Breslauer Str. 48  
D-76139 Karlsruhe



## License

Copyright © 2016 - 2020, Tobias Fleiter

SPDX-License-Identifier: CDLA-Permissive-1.0

License-Text: [Community Data License Agreement Permissive 1.0](https://cdla.io/permissive-1-0/)

This work is licensed under a Community Data License Agreement – Permissive – Version 1.0


## Acknowledgement
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.